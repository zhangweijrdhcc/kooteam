# kooteam 启动命令

- ./start.sh 启动应用
- ./start.sh upgrade 升级应用
- ./start.sh stop 关闭应用
- ./start.sh restart 重启应用

# 参数调整

- 打开 start.sh 文件修改 MEMORY 参数调整应用最大使用内存
- 应用默认最大内存 256M，可以根据服务器条件自行调整，最低不能低于 128M
